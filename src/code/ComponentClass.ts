export interface ComponentClass<T=any> {
	new(...p: any[]): T;
}