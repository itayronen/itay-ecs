import { ComponentsCollection } from "./ComponentsCollection";

export interface Entity {
    components: ComponentsCollection;
}

export class Entity implements Entity {
    public components = new ComponentsCollection();
}