import { TestSuite, TestParams } from "just-test-api";
import { expect } from "chai";
import { ComponentsCollection } from "./ComponentsCollection";
import { Component, ComponentMetadata, ComponentEntry } from "./Component";

module TestModule {
	export class Number { }
}

export class ParentClass { }

export class ChildClass extends ParentClass { }

export default function (suite: TestSuite): void {
	suite.describe("ComponentsCollection", suite => {
		suite.describe("add", suite => {
			suite.test("One item.", t => {
				t.arrange();
				let collection = new ComponentsCollection();
				let expected = 1;

				t.act();
				collection.add(expected);

				t.assert();
				let actual = collection.get(Number);
				expect(actual).to.equal(expected);
			});

			suite.test("Multiple different items.", t => {
				t.arrange();
				let collection = new ComponentsCollection();

				let expected1 = 1;
				let expected2 = true;
				let expected3 = new Date();

				t.act();
				collection.add(expected1);
				collection.add(expected2);
				collection.add(expected3);

				t.assert();
				let actual2 = collection.get(Boolean);
				let actual1 = collection.get(Number);
				let actual3 = collection.get(Date);

				expect(actual1).to.equal(expected1);
				expect(actual2).to.equal(expected2);
				expect(actual3).to.equal(expected3);
			});

			suite.test("Two items of the same type - throws.", t => {
				t.arrange();
				let collection = new ComponentsCollection();

				let item1 = 1;
				let item2 = 2;

				collection.add(item1);

				t.assert();
				expect(() => collection.add(item2)).to.throw();
			});

			suite.test("Two different classes with same name(different modules).", t => {
				t.arrange();
				let collection = new ComponentsCollection();
				let num = 1;
				let otherNumber = new TestModule.Number();

				t.act();
				collection.add(num);
				collection.add(otherNumber);

				t.assert();
				expect(collection.get(Number)).to.equal(num);
				expect(collection.get(TestModule.Number)).to.equal(otherNumber);
			});

			suite.test("ParentClass and deriving ChildClass.", t => {
				/*
					es6 changed the way classes and inheritance works.
					In es6, the constructors of classes also have a prototype property,
						that points to the parent class constructor.
					This will cause "static" properties (properties on the constructor) to be inheritable.
	
					This fucks saving the key of the component on the ctor, for classes declared in es6.
						Thats because when retrieving the key of a new component(class),
						instead of failing to find a key and creating a new one, the parent key will be found.
					    
					es5 code:
					It is possible to support es6 with es5 code by constructing a key that contains the class name.
						That will cause the key retrieval to access a different property from the parent's.
						But this will cause string concatenation for every request of a component.
				    
					es6 cod:
					Use map in the component collection instead of a key on the component's ctor.
					This is a cleaner approach anyway.
				*/
				t.arrange();
				let collection = new ComponentsCollection();

				let expected1 = new ParentClass();
				let expected2 = new ChildClass();

				t.act();
				collection.add(expected1);
				collection.add(expected2); // This would have fail.

				t.assert();
				let actual1 = collection.get(ParentClass);
				let actual2 = collection.get(ChildClass);

				expect(actual1).to.equal(expected1);
				expect(actual2).to.equal(expected2);
			});

			suite.test("One component for multiple keys.", t => {
				t.arrange();
				let collection = new ComponentsCollection();

				let expected: any = 1;

				t.act();
				collection.add(expected, Number, Date, String);

				t.assert();
				expect(collection.get(Number)).to.equal(expected);
				expect(collection.get(Date)).to.equal(expected);
				expect(collection.get(String)).to.equal(expected);
				expect(collection.get(Object)).to.be.undefined;
			});

			suite.describe("metadata", suite => {
				suite.test("One component with metadata defining multiple keys.", t => {
					t.arrange();
					let collection = new ComponentsCollection();

					let expectedComponent: Component = {} as Component;
					expectedComponent.componentMetadata = {};
					expectedComponent.componentMetadata.keys = [Number, Date, String];

					t.act();
					collection.add(expectedComponent);

					t.assert();
					let expected = expectedComponent as any;
					expect(collection.get(Number)).to.equal(expected);
					expect(collection.get(Date)).to.equal(expected);
					expect(collection.get(String)).to.equal(expected);
					expect(collection.get(Object)).to.be.undefined;
				});

				suite.test("One component with metadata defining multiple keys, and added with more keys - sign to all keys.", t => {
					t.arrange();
					let collection = new ComponentsCollection();

					let expectedComponent: Component = {} as Component;
					expectedComponent.componentMetadata = {}
					expectedComponent.componentMetadata.keys = [Number, Date];

					t.act();
					collection.add(expectedComponent, String);

					t.assert();
					let expected = expectedComponent as any;
					expect(collection.get(Number)).to.equal(expected);
					expect(collection.get(Date)).to.equal(expected);
					expect(collection.get(String)).to.equal(expected);
					expect(collection.get(Object)).to.be.undefined;
				});
			});

			suite.describe("entiries", suite => {
				suite.test("Component with metadata defining multiple entries.", t => {
					t.arrange();
					let collection = new ComponentsCollection();

					let expected1: Component = {} as Component;
					let expected2 = 3;
					expected1.componentMetadata = { entiries: [] };
					expected1.componentMetadata!.entiries!.push(new ComponentEntry(Number, expected1));
					expected1.componentMetadata!.entiries!.push(new ComponentEntry(Date, expected1));
					expected1.componentMetadata!.entiries!.push(new ComponentEntry(String, expected2));

					t.act();
					collection.add(expected1);

					t.assert();
					expect(collection.get(Number)).to.equal(expected1 as any);
					expect(collection.get(Date)).to.equal(expected1 as any);
					expect(collection.get(String)).to.equal(expected2 as any);
					expect(collection.get(Object)).to.be.undefined;
				});

				suite.test("Metadata defining entries and keys - throws.", t => {
					t.arrange();
					let collection = new ComponentsCollection();

					let expected1 = new Component();
					expected1.componentMetadata = { keys: [], entiries: [] };
					expected1.componentMetadata!.entiries!.push(new ComponentEntry(Number, expected1));
					expected1.componentMetadata!.keys!.push(Date);

					t.assert();
					expect(() => collection.add(expected1)).to.throw();
				});

				suite.test("Metadata defining entries, and added with additional keys - throws.", t => {
					t.arrange();
					let collection = new ComponentsCollection();

					let expected1 = new Component();
					expected1.componentMetadata = { entiries: [] };
					expected1.componentMetadata!.entiries!.push(new ComponentEntry(Number, expected1));

					t.assert();
					expect(() => collection.add(expected1, Date)).to.throw();
				});
			});
		});

		suite.describe("addList", suite => {
			suite.test("Simple.", t => {
				t.arrange();
				let collection = new ComponentsCollection();
				let expected1 = 1;
				let expected2 = "a";

				t.act();
				collection.addList([expected1, expected2]);

				t.assert();
				expect(collection.get(Number)).to.equal(expected1);
				expect(collection.get(String)).to.equal(expected2);
			});
		});

		suite.describe("addCollection", suite => {
			suite.test("Simple - Copies the keys not just the components.", t => {
				t.arrange();
				let collection1 = new ComponentsCollection();
				let collection2 = new ComponentsCollection();
				let expected1 = 1;
				let expected2 = "a";

				collection1.add(expected1);
				collection1.add(expected2, Date);

				t.act();
				collection2.addCollection(collection1);

				t.assert();
				expect(collection2.get(Number)).to.equal(expected1);
				expect(collection2.get(Date)).to.equal(expected2 as any);
				expect(collection2.get(String)).to.be.undefined;
			});
		});

		suite.describe("get", suite => {

			suite.test("By type", t => {
				t.arrange();
				let collection = new ComponentsCollection();
				let item = 2;

				collection.add(item);

				t.assert();
				expect(collection.get(Number)).to.equal(item);
			});

			suite.test("One components by multiple keys.", t => {
				t.arrange();
				let collection = new ComponentsCollection();

				let expected = new Number(1);

				collection.add(expected, Number, Date, String);

				t.assert();
				expect(collection.get(Number)).to.equal(expected);
				expect(collection.get(Date)).to.equal(expected as any);
				expect(collection.get(String)).to.equal(expected as any);
				expect(collection.get(Object)).to.be.undefined;
			});

			suite.test("No component found - return undefined.", t => {
				t.arrange();
				let collection = new ComponentsCollection();

				t.act();
				let actual = collection.get(Number);

				t.assert();
				expect(actual).to.be.undefined;
			});
		});

		suite.describe("remove", suite => {

			suite.test("Remove a simple component", t => {
				t.arrange();
				let collection = new ComponentsCollection();
				let item = 2;

				collection.add(item);

				t.act();
				collection.remove(item);

				t.assert();
				let actual = collection.get(Number);
				expect(actual).to.be.undefined;
			});

			suite.test("Remove one of two different classes with same name(different modules).", t => {
				t.arrange();
				let collection = new ComponentsCollection();
				let item = 2;
				let item2 = new TestModule.Number();

				collection.add(item);
				collection.add(item2);

				t.act();
				collection.remove(item);

				t.assert();
				expect(collection.get(Number)).to.be.undefined;
				expect(collection.get(TestModule.Number)).to.equal(item2);
			});

			suite.test("Remove component with multiple keys.", t => {
				t.arrange();
				let collection = new ComponentsCollection();
				let item = 2;

				collection.add(item, Number, Date);

				t.act();
				collection.remove(item);

				t.assert();
				expect(collection.get(Number)).to.be.undefined;
				expect(collection.get(Date)).to.be.undefined;
			});

			suite.test("Remove component with metadata entries - removes all entries.", t => {
				t.arrange();
				let collection = new ComponentsCollection();
				let component: Component = { componentMetadata: { entiries: [] } };
				component.componentMetadata!.entiries!.push({ key: Number, value: 3 });
				component.componentMetadata!.entiries!.push({ key: String, value: "a" });

				collection.add(component);

				t.act();
				collection.remove(component);

				t.assert();
				expect(collection.get(Number)).to.be.undefined;
				expect(collection.get(String)).to.be.undefined;
			});
		});

		suite.describe("contains", suite => {

			suite.test("When input component is in the collection, returns true.", t => {
				t.arrange();
				let collection = new ComponentsCollection();
				let item = 2;

				collection.add(item);

				t.act();
				let actual: boolean = collection.contains(Number);

				t.assert();
				expect(actual).to.be.true;
			});

			suite.test("When input component is NOT in the collection, returns false.", t => {
				t.arrange();
				let collection = new ComponentsCollection();
				let item = 2;

				collection.add(item);

				t.act();
				let actual: boolean = collection.contains(Date);

				t.assert();
				expect(actual).to.be.false;
			});
		});

		suite.describe("containsAny", suite => {

			suite.test("Input of an existing components, returns true.", t => {
				t.arrange();
				let collection = new ComponentsCollection();
				let item = 2;

				collection.add(item);

				t.act();
				let actual: boolean = collection.containsAny([Date, Number]);

				t.assert();
				expect(actual).to.be.true;
			});

			suite.test("Input of an unexisting components, returns false.", t => {
				t.arrange();
				let collection = new ComponentsCollection();
				let item = 2;

				collection.add(item);

				t.act();
				let actual: boolean = collection.containsAny([Date, String]);

				t.assert();
				expect(actual).to.be.false;
			});
		});

		suite.describe("containsAll", suite => {

			suite.test("Input of an existing components, returns true.", t => {
				t.arrange();
				let collection = new ComponentsCollection();

				collection.add(2);
				collection.add(new Date());

				t.act();
				let actual: boolean = collection.containsAll([Date, Number]);

				t.assert();
				expect(actual).to.be.true;
			});

			suite.test("Input of an unexisting components, returns false.", t => {
				t.arrange();
				let collection = new ComponentsCollection();
				let item = 2;

				collection.add(item);

				t.act();
				let actual: boolean = collection.containsAll([Date, Number]);

				t.assert();
				expect(actual).to.be.false;
			});
		});

		suite.describe("Special", suite => {
			class StaticBody {
				public x = 0;
				public y = 0;
			}
			class Renderable {
				public posX = 0;
				public posY = 0;
			}
			class MyPositionComponent {
				public x = 0;
				public y = 0;

				public get posX() {
					return this.x;
				}

				public get posY() {
					return this.y;
				}
			}

			suite.test("Mixing components.", t => {
				t.arrange();
				let collection = new ComponentsCollection();
				let component = new MyPositionComponent();
				component.x = 1;
				component.y = 2;

				collection.add(component, StaticBody, Renderable);

				t.act();
				let body = collection.get(StaticBody);
				let renderable = collection.get(Renderable);

				t.assert();
				expect(body.x).to.equal(1);
				expect(body.y).to.equal(2);
				expect(renderable.posX).to.equal(1);
				expect(renderable.posY).to.equal(2);
			});
		});

		suite.describe("getHashCode", suite => {
			suite.test("Add and remove components changes the hash code symetrically.", t => {
				t.arrange();
				let collection = new ComponentsCollection();
				let num = 1;
				let str = "a";
				let bool = true;

				t.act();
				collection.add(num);

				let hashNumber = collection.getHashCode();

				collection.add(str);
				collection.add(bool);

				let hashNumberStringBoolean = collection.getHashCode();

				collection.remove(num);

				let hashStringBoolean = collection.getHashCode();

				t.assert();
				expect(hashNumber).not.to.equal(hashStringBoolean);
				expect(hashNumber).not.to.equal(hashNumberStringBoolean);
				expect(hashStringBoolean).not.to.equal(hashNumberStringBoolean);

				collection = new ComponentsCollection();
				collection.add(num);
				expect(collection.getHashCode()).to.equal(hashNumber);

				collection = new ComponentsCollection();
				collection.addList([num, str, bool]);
				expect(collection.getHashCode()).to.equal(hashNumberStringBoolean);

				collection = new ComponentsCollection();
				collection.addList([str, bool]);
				expect(collection.getHashCode()).to.equal(hashStringBoolean);
			});

			suite.test("Many components with long names and one with a short name (an actual bug).", t => {
				class asdakufheisuKHASDefhalHAIUHfas89fsfksdh { }
				class oidf8934ujlaHASASDIYGSD9yasd98yas89dyh23IHISas9dy { }
				class dkshfisd9s8ydf9sduihfsdi9f8hsd9fsdnof8hsdifsdfnsdfui { }
				class ferg45t8ywerjhn34y8hnve8yvhn8gy45hn8vy54vhn8g5y7vn45 { }
				class vg45m54opmvw489pp84m9gv8w4gvw4ym9pg5vwg4pmgy49pwgymw49p { }
				class UHFNRENGVU9VUEPSRIOUGNSEROIGY7NSERGO8ER7GYNSEOGERHNOSSG { }
				class AAA { }

				t.arrange();
				let collection = new ComponentsCollection();
				let a = new asdakufheisuKHASDefhalHAIUHfas89fsfksdh();
				let b = new oidf8934ujlaHASASDIYGSD9yasd98yas89dyh23IHISas9dy()
				let c = new dkshfisd9s8ydf9sduihfsdi9f8hsd9fsdnof8hsdifsdfnsdfui()
				let d = new ferg45t8ywerjhn34y8hnve8yvhn8gy45hn8vy54vhn8g5y7vn45()
				let e = new vg45m54opmvw489pp84m9gv8w4gvw4ym9pg5vwg4pmgy49pwgymw49p()
				let f = new UHFNRENGVU9VUEPSRIOUGNSEROIGY7NSERGO8ER7GYNSEOGERHNOSSG()
				let g = new AAA()

				t.assert();
				let hashNumber1 = collection.getHashCode();

				for (let item of [a, b, c, d, e, f, g]) {
					collection.add(item);

					let hashNumber2 = collection.getHashCode();

					expect(hashNumber1).to.not.equal(hashNumber2);

					hashNumber1 = hashNumber2;
				}
			});
		});
	});
}