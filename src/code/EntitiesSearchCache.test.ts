import { TestSuite, TestParams } from "just-test-api";
import { expect } from "chai";
import { EntitiesSearchCache } from "./EntitiesSearchCache";
import { EntitiesObservation } from "./EntitiesObservation";
import { EntitiesCollection } from "./EntitiesCollection";
import { Entity } from "./Entity";
import { ComponentsCollection } from "./ComponentsCollection";

class NumberBooleanEntity implements Entity {
	public components: ComponentsCollection = new ComponentsCollection();
	public number = 42;

	constructor() {
		this.components.add(this.number);
		this.components.add(true);
	}
}

class NumberStringEntity implements Entity {
	public components: ComponentsCollection = new ComponentsCollection();

	constructor() {
		this.components.add(42);
		this.components.add("asd");
	}
}

export default function (suite: TestSuite): void {
	suite.describe("EntitiesSearchCache", suite => {
		suite.test("When initialized, then has the filter matching entities.", t => {
			t.arrange();
			let collection = new EntitiesCollection();
			let entity1 = new NumberBooleanEntity();
			let entity2 = new NumberBooleanEntity();
			collection.add(entity1);
			collection.add(entity2);
			collection.add(new NumberStringEntity());

			t.act();
			let cache = EntitiesSearchCache.from(collection).componentsContainsAll([Number, Boolean]);

			t.assert();
			expect(cache.size).to.equal(2);
			expect(cache.has(entity1)).to.be.true;
			expect(cache.has(entity2)).to.be.true;
		});

		suite.test("When matching entity added to source, then its in the cache.", t => {
			t.arrange();
			let collection = new EntitiesCollection();
			let cache = EntitiesSearchCache.from(collection).componentsContainsAll([Number, Boolean]);

			t.act();
			let entity1 = new NumberBooleanEntity();
			collection.add(entity1);

			t.assert();
			expect(cache.size).to.equal(1);
			expect(cache.has(entity1)).to.be.true;
		});

		suite.test("Added entity triggers callback.", t => {
			t.arrange();
			let collection = new EntitiesCollection();

			let cache = EntitiesSearchCache.from(collection).componentsContainsAny([Number]);

			let called = false;
			cache.onEntityAdded = e => called = true;

			t.act();
			let entity1 = new NumberBooleanEntity();
			collection.add(entity1);

			t.assert();
			expect(called).to.be.true;
		});

		suite.test("When entity matching the filter is removed, then its removed from cache.", t => {
			t.arrange();
			let collection = new EntitiesCollection();
			let entity1 = new NumberBooleanEntity();
			collection.add(entity1);

			let cache = EntitiesSearchCache.from(collection).componentsContainsAll([Number, Boolean]);
			expect(cache.size).to.equal(1);

			t.act();
			collection.remove(entity1);

			t.assert();
			expect(cache.size).to.equal(0);
		});

		suite.test("When an entity that no longer matches the filter is removed, then its removed from cache.", t => {
			t.arrange();
			let collection = new EntitiesCollection();
			let entity1 = new NumberBooleanEntity();
			collection.add(entity1);

			let cache = EntitiesSearchCache.from(collection).componentsContainsAll([Number, Boolean]);
			expect(cache.size).to.equal(1);

			t.act();
			entity1.components.remove(entity1.number);
			collection.remove(entity1);

			t.assert();
			expect(cache.size).to.equal(0);
		});

		suite.test("Removed entity triggers callback.", t => {
			t.arrange();
			let collection = new EntitiesCollection();
			let cache = EntitiesSearchCache.from(collection).componentsContainsAny([Number]);

			let entity1 = new NumberBooleanEntity();
			collection.add(entity1);

			let called = false;
			cache.onEntityRemoved = e => called = true;

			t.act();
			collection.remove(entity1);

			t.assert();
			expect(called).to.be.true;
		});

		suite.test("Removed entity from source that wasn't in cache, does NOT trigger callback.", t => {
			t.arrange();
			let collection = new EntitiesCollection();
			let cache = EntitiesSearchCache.from(collection).componentsContainsAny([String]);

			let entity1 = new NumberBooleanEntity();
			collection.add(entity1);

			let called = false;
			cache.onEntityRemoved = e => called = true;

			t.act();
			collection.remove(entity1);

			t.assert();
			expect(called).to.be.false;
		});

		suite.test("When disposed, and entity that was in cache is removed, " +
			"then no error is logged from the removed event (it was a bug).", t => {
				t.arrange();
				let collection = new EntitiesCollection();
				let cache = EntitiesSearchCache.from(collection).componentsContainsAny([String]);

				let entity1 = new NumberStringEntity();
				collection.add(entity1);

				cache.dispose();

				t.act();
				collection.remove(entity1);
			});
	});
}