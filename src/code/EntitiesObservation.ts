import { SignableEvent1 } from "itay-events";
import { Entity } from "./Entity";
import { EntitiesFilter } from "./EntitiesFilter";

export interface EntityAddedArgs {
	readonly entity: Entity,
	readonly onRemoved: SignableEvent1<EntityRemovedArgs>;
}

export interface EntityRemovedArgs {
	readonly entity: Entity;
}

export class EntitiesObservation {
	public added: (entity: EntityAddedArgs) => void = function () { };

	public filter: EntitiesFilter = EntitiesFilter.passAll();
}
