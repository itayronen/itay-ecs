# itay-ecs
A generic entity component system in typescript.

The goal of this library is to allow combining multiple independent libraries to create games.  
For example: Use a rendering library like "pixi", "threejs", "babylon" etc. With a physics system like "Box2D" or "p2.js".

## Install
`npm install --save itay-ecs`

## Concept
### Component 
A piece of information (of an entity) on a specific topic.  
> For example, A person entity might have a "HeightComponent" that holds a number to represent the person's height.

Components can be used to mark an entity, without having any additional properties.  
> For example, A person entity can hold a "TeacherComponent" to be marked as a teacher.   

### Entity
A logical representatin of an object. Its data is contained in the components it holds.

### System
Contains logic that acts on entities, using the entities components.

### In this library  
Every class can be a component, from a simple number to a complex class.

A component is usualy identified by its class. To be more exact, his constructor function (well thats javascript).
> A component can also be referenced by other and multiple constructors. See the advanced section.

The library allows you to choose what to use and how.  
The library provides:
* ComponentsCollection class - to hold the entity's components.
* EntitiesCollection class - to hold entities (objects that implements Entity). 

When it comes to systems, implement as you like.  
Check the usage sections to learn about classes and functionality that will make your life very simple when implementin a system.  

## Basic Usage
### ComponentsCollection
```ts
let collection = new ComponentsCollection();
let component = 1; // Component of type Number.

collection.add(component);

let myNumber: number = collection.get(Number);
```

### EntitiesCollection

```ts
// Some example entity
class TankEntity implements Entity {
    public components: ComponentsCollection = new ComponentsCollection();

    constructor() {
        this.components.add(new GunsComponent(7));
    }
}

// Some example component
class GunsComponent {
    public power: number = 1;

    constructor(power?: number) {
        if (power) {
            this.power = power;
        }
    }
}

// Using EntitiesCollection
let entities = new EntitiesCollection();
entities.add(new TankEntity());

let myShootingEntities: Entity[] = collection.getByComponent(GunsComponent);
console.log(myShootingEntities.length); // 1
```

## Advanced Usage
### Shared Components
Usualy a component is tailor made for a specific system.  
That raises some common problems:
* Sharing information between systems is hard, and might cause **coupling**.
* Mixing third-party libraries is hard.  

> For example, a "PositionComponent" is used by the physics-system and by the render-system.

This library offers a simple solution: Multiple component keys.

Given the classes:

```ts
class StaticBodyComponent { // A physics system component.
    public x: number;
    public y: number;
}

class RenderablePositionComponent { // A render system component.
    public posX: number;
    public posY: number;
}

class MyPositionComponent { // My game component.
    public x: number;
    public y: number;

    public get posX() {
        return this.x;
    }

    public get posY() {
        return this.y;
    }
}
```
We can:

```ts
let collection = new ComponentsCollection();
let myComponent = new MyPositionComponent();
myComponent.x = 1;
myComponent.y = 2;

collection.add(myComponent, StaticBodyComponent, RenderablePositionComponent);
// Or: collection.add(myComponent, [StaticBodyComponent, RenderablePositionComponent]);

let staticBody = collection.get(StaticBodyComponent); // x: 1, y: 2
let renderable = collection.get(RenderablePositionComponent); // posX: 1, posY: 2
```

### Observe entities
Observing entities collection is a simple and usefull way to keep track of a set of entities.  
One use case is using observation to keep track of entities for a system.  

```ts
let collection = new EntitiesCollection();

let addedTriggered = false;

let observation = new EntitiesObservation();
observation.added = entity => addedTriggered = true;
observation.filter = EntitiesFilter.componentsContainsAny([GunsComponent, SomeOtherComponent])

collection.addObservation(observation);

collection.add(new TankEntity()); // TankEntity has GunsComponent

console.log(addedTriggered); // true
```

### EntitiesSearchCache
EntitiesSearchCache class holds a list of filtered entities from an EntitiesCollection.  

Its implemented using the observation feature.  

You can use this class to hold entities for a system. This will be the simplest approach.  

```ts
let collection = new EntitiesCollection();

let entityToRemove = new NumberBooleanEntity();
collection.add(entityToRemove); // Will be selected by the search.
collection.add(new NumberStringEntity()); // Will NOT be selected by the search.

let cache = EntitiesSearchCache.from(collection).componentsContainsAll([Number, Boolean]); // Cache contains 1 entity.

collection.add(new NumberBooleanEntity()); // Selected by the search. Cache contains 2 entities.
collection.remove(entityToRemove); // Cache contains 1 entity.

for (let entity of cache.entities){
    console.log(entity);
}
```


