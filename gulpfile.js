"use strict";
let gulp = require("gulp");
let ts = require('gulp-typescript');
let del = require("del");
let changed = require("itay-gulp-changed");
let merge = require("merge2");
let reexportFolder = require("gulp-reexport").createReexportsFileOnFileSystem;
var gulpif = require('gulp-if');
let tsPackage = require("gulp-ts-package").default;
let manglePrivate = require("ts-mangle-private").default;
let uglify = require("gulp-uglify-es").default;

let config = {
	tsconfig: "./src/tsconfig.json",
	tsGlob: "./src/**/*.ts",
	packageEntriesGlob: ["./src/index.ts"],
	mainModule: "index",
	dest: "./lib",
	bundles: "./bundles",
	packageName: require("./package.json").name,
	get compilerOptions() { return require(this.tsconfig).compilerOptions; }
};

gulp.task("build", ["reexport"], function () {
	let tsProject = ts.createProject(config.tsconfig, { declaration: true });

	let tsResult = gulp.src(config.tsGlob)
		.pipe(changed())
		.pipe(tsProject());

	tsResult.on("error", () => {
		changed.reset();
		throw "Typescript build failed.";
	});

	return merge([
		tsResult.js.pipe(gulp.dest(config.dest)),
		tsResult.dts.pipe(gulp.dest(config.dest)),
	]);
});

gulp.task("reexport", function () {
	let outFileName = config.mainModule + ".ts";
	let glob = ["./src/**/*.ts", "!**/*.test.ts", `!**/${outFileName}`];

	return reexportFolder({
		glob: glob,
		outDir: "./src/",
		outFileName: outFileName,
		gulp: gulp,
		changed: changed
	});
});

gulp.task("package", () => {
	let compilerOptions = config.compilerOptions;

	let streams = [];

	["amd"/*, "system"*/].forEach(moduleFormat => {
		["standard", "min"].forEach(type => {
			compilerOptions.module = moduleFormat;
			compilerOptions.outFile = type == "min" ?
				`bundle.${moduleFormat}.min.js` :
				`bundle.${moduleFormat}.js`;

			let tsProject = ts.createProject(compilerOptions);

			streams.push(gulp.src(config.packageEntriesGlob)
				.pipe(gulpif(type == "min", manglePrivate()))
				.pipe(tsProject())
				.pipe(tsPackage({
					name: config.packageName,
					mainModule: config.mainModule,
					minify: { enabled: true }
				}))
				.pipe(gulpif(type == "min", uglify()))
				.pipe(gulp.dest(config.bundles))
			);
		});
	});

	return merge(streams);
});

gulp.task("clean", () => del([config.dest, config.bundles, "./.localStorage"]));
// "use strict"

// let gulp = require("gulp");
// let path = require("path");
// let packageJson = require("./package.json");
// let itayTasks = require("itay-tasks").default;

// let codeDir = "./src/code";

// function exportTsFolder(dir, outDir, outFileName) {
//     let glob = [dir + "/**/*.ts", "!**/*.test.ts", "!**/tests.ts", `!**/${outFileName}`];

//     return itayTasks.reexportFolder(glob, outDir, outFileName, outFileName + "-changed-key");
// }

// function exportTestsFolder(dir, outDir, outFileName) {
//     let glob = [dir + "**/*.test.ts"];

//     return itayTasks.reexportFolder(glob, outDir, outFileName, "tests-changed-key");
// }

// function prePackage() {
//     let rootModuleFileName = path.basename(packageJson.config.rootModule);

//     return exportTsFolder(codeDir, codeDir, rootModuleFileName);
// }

// function preBuild() {
//     return exportTestsFolder(codeDir, codeDir, "tests.ts");
// }

// itayTasks.createTasks({ gulp: gulp, preBuild: preBuild, packageJson: packageJson, prePackage: prePackage });
